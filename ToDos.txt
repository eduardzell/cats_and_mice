Bugs:
* Baby mouse does not follow mother mouse - added destruction of baby mouse as temporal solution
* Eating cheese should take some time

Open Features:
* UI at beginning
* Game Over UI
* baby mouse is distracted by cheese and does not follow mother mouse before eating it up